# finite volume implementation of wave dynamics

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.optimize import fsolve, root
import numdifftools as nd
import scipy.io as sio
from numba import jit
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
from math import floor

TYPE_SLACKBUS = 0
TYPE_GENERATOR = 1
TYPE_LOAD = 2

W_IDX = 0
P_IDX = 1
V_IDX = 2

# RUNTIME FLAGS
VERIFY_JACOBIAN = False
INERTIA_HETEROGENEOUS = False
FD_JACOBIAN = False
VERBOSE = True

def matprint(mat, fmt="g"):
# code from:
# https://gist.github.com/lbn/836313e283f5d47d2e4e
    col_maxes = [max([len(("{:"+fmt+"}").format(x)) for x in col]) for col in mat.T]
    for x in mat:
        for i, y in enumerate(x):
            print(("{:"+str(col_maxes[i])+fmt+"}").format(y), end="  ")
        print("")

def load_matpower(mat_file, volt=False):

    """
        The files loaded here are the result executing the following commands in MATPOWER
        mpc = loadcase('casefile.m')
        save('casefile.mat', mpc)
    """

    case = sio.loadmat(mat_file)

    basemva = case['mpc'][0][0][1][0][0]
    mat_buses = np.array(case['mpc'][0][0][2])
    mat_gens = np.array(case['mpc'][0][0][3])
    mat_branches = np.array(case['mpc'][0][0][4])
        

    nbus = mat_buses.shape[0]
    nbranch = mat_branches.shape[0]
    ngens = mat_gens.shape[0]
    mat_to_int = {}
 
    net = Network(voltage=volt)

    for i in range(nbus):
        if int(mat_buses[i, 1]) == 1:
            bus_type = "load"
        elif int(mat_buses[i, 1]) == 2:
            bus_type = "generator"
        elif (mat_buses[i, 1]) == 3:
            bus_type = "slack"
        else:
            raise Exception("Bus type not recognized.")

        p_load = -mat_buses[i, 2]/100.0
        mat_to_int[mat_buses[i, 0]] = i
        vmag_bus = mat_buses[i, 7]

        net.add_bus(i, vmag_bus, p_load, bus_type)

    for i in range(ngens):
        bus = mat_to_int[int(mat_gens[i, 0])]
        net.buses[bus].pg += mat_gens[i, 1]/100.0

    for i in range(nbranch):
        fr_internal = mat_to_int[int(mat_branches[i, 0])]
        to_internal = mat_to_int[int(mat_branches[i, 1])]

        # we'll assume homogeneous data for now
        ws = 1.0
        h = 1.0
        pg = 0.0
        v = 1.0
        b = 400.0
        lon = 5.0
        net.add_branch(ws, h, pg, v, b, lon, fr_internal, to_internal)

    return net

# Data structures

class Network:

    def __init__(self, voltage=False):
        self.branches = []
        self.buses = []
        self.ptr = []
        self.voltage = voltage
        self.dx = -1

        if voltage:
            self.NFIELD = 3
        else:
            self.NFIELD = 2

    def add_branch(self, ws, h, pg, v, b, lon, fr_bus, to_bus):
        idx = len(self.branches)
        self.branches.append(Branch(idx, ws, h, pg, v, b, lon, fr_bus, to_bus))

    def add_bus(self, idx, vmag, pg, TYPE):
        self.buses.append(Node(idx, vmag, pg, TYPE))

    def assemble(self, dx):

        NFIELD = self.NFIELD

        self.dx = dx
        self.dof = 0
        for bus in self.buses:
            # each bus has 2 dof.
            self.ptr.append(self.dof)
            self.dof += NFIELD

        for branch in self.branches:
            self.ptr.append(self.dof)
            npoints = math.floor(branch.lon/dx) + 1
            branch.points = npoints
            self.dof += NFIELD*npoints

        self.nbranch = len(self.branches)
        self.nbus = len(self.buses)

        # each bus has a list of the branches that its connected to
        for branch in self.branches:
            idx_branch = branch.idx
            fr_bus = branch.fr
            to_bus = branch.to
            self.buses[fr_bus].branches_fr.append(idx_branch)
            self.buses[to_bus].branches_to.append(idx_branch)


        # port data to arrays for vectorization

        self.bus_idx = np.zeros(self.nbus, dtype=np.int64)
        self.bus_pg = np.zeros(self.nbus, dtype=np.double)
        self.bus_vmag = np.zeros(self.nbus, dtype=np.double)
        self.bus_type = np.zeros(self.nbus, dtype=np.int64)
        self.bus_ptr = np.zeros(self.nbus, dtype=np.int64)

        for i in range(len(self.buses)):
            self.bus_idx[i] = self.buses[i].idx
            self.bus_pg[i] = self.buses[i].pg
            self.bus_vmag[i] = self.buses[i].vmag
            self.bus_type[i] = self.buses[i].type
            self.bus_ptr[i] = self.get_busptr(self.buses[i].idx)
        
        self.br_idx = np.zeros(self.nbranch, dtype=np.int64)
        self.br_ptr = np.zeros(self.nbranch, dtype=np.int64)
        self.br_fr = np.zeros(self.nbranch, dtype=np.int64)
        self.br_to = np.zeros(self.nbranch, dtype=np.int64)
        self.br_points = np.zeros(self.nbranch, dtype=np.int64)
        
        self.br_ws = np.zeros(self.nbranch, dtype=np.double)
        self.br_h = np.zeros(self.nbranch, dtype=np.double)
        self.br_b = np.zeros(self.nbranch, dtype=np.double)
        self.br_v = np.zeros(self.nbranch, dtype=np.double)
        self.br_pg = np.zeros(self.nbranch, dtype=np.double)
        self.br_lon = np.zeros(self.nbranch, dtype=np.double)

        for i in range(len(self.branches)):
            self.br_idx[i] = self.branches[i].idx
            self.br_ptr[i] = self.get_branchptr(self.branches[i].idx)
            self.br_fr[i] = self.branches[i].fr
            self.br_to[i] = self.branches[i].to
            self.br_points[i] = self.branches[i].points
            
            self.br_ws[i] = self.branches[i].ws
            self.br_h[i] = self.branches[i].h
            self.br_b[i] = self.branches[i].b
            self.br_v[i] = self.branches[i].v
            self.br_pg[i] = self.branches[i].pg
            self.br_lon[i] = self.branches[i].lon

    def get_busptr(self, bus_idx):
        return self.ptr[bus_idx]

    def get_branchptr(self, branch_idx):
        return self.ptr[self.nbus + branch_idx]

    def get_branch_field(self, br, field_id):
        br_ptr = self.br_ptr[br]
        npoints = self.br_points[br]
        return [br_ptr + field_id + self.NFIELD*i for i in range(npoints)]


class Branch:

    def __init__(self, idx, ws, h, pg, v, b, lon, fr_bus, to_bus):

        self.idx = idx
        self.ws = ws # Synchronous frequency (a constant)
        self.h = h # Distributed inertia (will be a function)
        self.pg = pg #line power (we'll do 0 from now unless distributed)
        self.v = v #line voltage
        self.b = b # line reactance
        self.lon = lon # longitude
        self.fr = fr_bus
        self.to = to_bus

        # wave parameters
        self.lam = np.sqrt((v*v*b*ws)/(2*h))
        self.s = np.sqrt((pg*np.sqrt(ws**3))/(8*v**2*b*h**3))

        # construct FV matrices
        q_temp = np.sqrt(ws/(2*v*v*b*h))
        self.Q = np.array(([1, q_temp], [1, -q_temp]))
        self.Qinv = np.linalg.inv(self.Q)

        # integration parameters
        self.points = 0

    def __str__(self):
        msg = "Branch connected from node %d to node %d.\n" % (self.fr,
            self.to)
        msg += "\tlongitude: %g (m).\n\treactance: %g (p.u/m)." % (self.lon, self.b)

        return msg


class Node:

    def __init__(self, idx, vmag, pg, TYPE):
        self.idx = idx
        self.pg = pg
        self.vmag = vmag
        self.branches_fr = []
        self.branches_to = []

        if TYPE == "slack":
            self.type = TYPE_SLACKBUS
            self.h = 1.5
        elif TYPE == "generator":
            self.type = TYPE_GENERATOR
            self.h = 1.5
        elif TYPE == "load":
            self.type = TYPE_LOAD
            self.h = 0.1
        else:
            raise("Bus type not defined")

# initialization

@jit(nopython=True, cache=True)
def residual_finit_imp(F, x, bus_idx, bus_type, bus_pg, bus_vmag, bus_ptr,
        br_idx, br_ptr, br_fr, br_to, br_points, vmode, NFIELD):
    for i in range(len(bus_idx)):
        ptr = bus_ptr[i]
        w = x[ptr + W_IDX]
        p = x[ptr + P_IDX]
        if vmode:
            vmag = x[ptr + V_IDX]

        if bus_type[i] == TYPE_SLACKBUS:
            F[ptr] = w # we set w_slack = 0
            F[ptr + 1] = p # p will be unknown (slack)
            if vmode:
                F[ptr + 2] = vmag - bus_vmag[i]
        elif bus_type[i] == TYPE_LOAD or bus_type[i] == TYPE_GENERATOR:
            F[ptr] = p + bus_pg[i]
            F[ptr + 1] = -p
            if vmode:
                F[ptr + 2] = vmag - bus_vmag[i]
        else:
            raise Exception("Node type not recognized")
    
    for br in range(len(br_idx)):
        ptr = br_ptr[br]
        ptr_fr = bus_ptr[br_fr[br]]
        ptr_to = bus_ptr[br_to[br]]

        #Fill inner-points of the branch
        for i in range(br_points[br] - 1):
            F[ptr + NFIELD*i] = x[ptr + NFIELD*(i + 1) + W_IDX] - x[ptr + NFIELD*i + W_IDX]
            F[ptr + NFIELD*i + 1] = x[ptr + NFIELD*(i + 1) + P_IDX] - x[ptr + NFIELD*i + P_IDX]
            

        if vmode:
            for i in range(br_points[br] - 2):
                F[ptr + NFIELD*i + 2] = (x[ptr + NFIELD*(i + 1) + V_IDX] - 
                    0.5*(x[ptr + NFIELD*(i) + V_IDX] + x[ptr + NFIELD*(i + 2) + V_IDX]))

        # evaluate upstream boundary
        w_fr = x[ptr_fr + W_IDX]
        v_fr = x[ptr_fr + V_IDX]
        F[ptr + NFIELD*(br_points[br] - 1)] = x[ptr + W_IDX] - w_fr

        if bus_type[br_fr[br]] == TYPE_SLACKBUS:
            F[ptr_fr + 1] -= x[ptr + P_IDX]
        else:
            F[ptr_fr + 1] -= x[ptr + P_IDX]

        if vmode:
            F[ptr + NFIELD*(br_points[br] - 2) + 2] = x[ptr + V_IDX] - v_fr

        # evaluate downstream boundary
        w_to = x[ptr_to + W_IDX]
        v_to = x[ptr_to + V_IDX]
        F[ptr + NFIELD*(br_points[br] - 1) + 1] = x[ptr + NFIELD*(br_points[br] - 1) + W_IDX] - w_to

        if bus_type[br_to[br]] == TYPE_SLACKBUS:
            F[ptr_to + 1] -= x[ptr + NFIELD*(br_points[br] - 1) + P_IDX]
        else:
            F[ptr_to] += x[ptr + NFIELD*(br_points[br] - 1) + P_IDX]
        
        if vmode:
            F[ptr + NFIELD*(br_points[br] - 1) + 2] = x[ptr + NFIELD*(br_points[br] - 1) + V_IDX] - v_to

def residual_finit(x, net):

    F = np.zeros_like(x)

    residual_finit_imp(F, x, net.bus_idx, net.bus_type, net.bus_pg, net.bus_vmag,
            net.bus_ptr, net.br_idx, net.br_ptr, net.br_fr, net.br_to, net.br_points, net.voltage, net.NFIELD)

    return F


@jit(nopython=True, cache=True)
def residual_lw_imp(F, x, state, dt, dx, vmode, NFIELD, bus_idx, bus_type, bus_pg, bus_vmag, bus_ptr,
        br_idx, br_ptr, br_fr, br_to, br_points,
        br_ws, br_h, br_b, br_v, br_pg, br_lon):
    
    for i in range(len(bus_idx)):
        ptr = bus_ptr[i]
        p = x[ptr + P_IDX]        
        vmag = x[ptr + V_IDX]        
        F[ptr] = p + bus_pg[i]
        F[ptr + 1] = -p
        #print("Bus {0}. F[ptr]={1}. F[ptr + 1]={2}. p={3}. bus_pg[i]={4}".format(i, F[ptr], F[ptr + 1], p, bus_pg[i]))
        if vmode:
            F[ptr + 2] = vmag - bus_vmag[i]
    
    for br in range(len(br_idx)):
        ptr = br_ptr[br]
        ptr_fr = bus_ptr[br_fr[br]]
        ptr_to = bus_ptr[br_to[br]]
        npoints = br_points[br]

        ws = br_ws[br]
        h = br_h[br]
        b = br_b[br]
        v = br_v[br]
        pg = br_pg[br]
        lon = br_lon[br]

        for i in range(npoints):
            if vmode:
                p = state[ptr + NFIELD*i + P_IDX]
                w = state[ptr + NFIELD*i + W_IDX]
                v = state[ptr + NFIELD*i + V_IDX]
                p_t = x[ptr + NFIELD*i + P_IDX]
                w_t = x[ptr + NFIELD*i + W_IDX]
                v_t = x[ptr + NFIELD*i + V_IDX]

                if i == 0:
                    w_boundary = x[ptr_fr + W_IDX] # boundary frequency at time t + 1
                    v_boundary = x[ptr_fr + V_IDX] # boundary voltage at time t + 1
                    F[ptr + NFIELD*i + W_IDX] = w_t - w_boundary # Enforce continuity
                    F[ptr + NFIELD*i + V_IDX] = v_t - v_boundary # Enforce continuity

                    # Contribution to enforce conservation of P
                    F[ptr_fr + 1] -= p_t

                    factor = np.sqrt(ws/(2*v*v*h*b))
                    w_b = state[ptr + NFIELD*(i + 1) + W_IDX]
                    p_b = state[ptr + NFIELD*(i + 1) + P_IDX]
                    F[ptr + NFIELD*i + P_IDX] = w_t - w_b - factor*(p_t - p_b)

                elif i == npoints - 1:
                    w_boundary = x[ptr_to + W_IDX]
                    v_boundary = x[ptr_to + V_IDX] # boundary voltage at time t + 1
                    F[ptr + NFIELD*i + W_IDX] = w_t - w_boundary # Enforce continuity
                    F[ptr + NFIELD*i + V_IDX] = v_t - v_boundary # Enforce continuity
                    
                    # Contribution to enforce conservation of P
                    F[ptr_to] += p_t

                    factor = np.sqrt(ws/(2*v*v*h*b))
                    w_a = state[ptr + NFIELD*(i - 1) + W_IDX]
                    p_a = state[ptr + NFIELD*(i - 1) + P_IDX]
                    F[ptr + NFIELD*i + P_IDX] = w_t - w_a + factor*(p_t - p_a)

                else:
                    p_b = state[ptr + NFIELD*(i - 1) + P_IDX]
                    w_b = state[ptr + NFIELD*(i - 1) + W_IDX]
                    p_f = state[ptr + NFIELD*(i + 1) + P_IDX]
                    w_f = state[ptr + NFIELD*(i + 1) + W_IDX]
                    
                    v_t_b = x[ptr + NFIELD*(i - 1) + V_IDX]
                    v_t_f = x[ptr + NFIELD*(i + 1) + V_IDX]

                    dP = (p_f - p_b)/(2*dx)
                    dW = (w_f - w_b)/(2*dx)

                    w_hat = 0.5*(w_b + w_f)
                    p_hat = 0.5*(p_b + p_f)

                    F[ptr + NFIELD*i + P_IDX] = w_t - (w_hat + dt*(ws/(2*h))*(pg - dP))
                    F[ptr + NFIELD*i + W_IDX] = p_t - (p_hat - dt*(v**2)*b*dW + 2*(v_t - v)*(p/v))
                    F[ptr + NFIELD*i + V_IDX] = v_t_b - 2*v_t  + v_t_f

            else:
                p = state[ptr + NFIELD*i + P_IDX]
                w = state[ptr + NFIELD*i + W_IDX]
                p_t = x[ptr + NFIELD*i + P_IDX]
                w_t = x[ptr + NFIELD*i + W_IDX]

                if i == 0:
                    w_boundary = x[ptr_fr + W_IDX] # boundary frequency at time t + 1
                    F[ptr + NFIELD*i + W_IDX] = w_t - w_boundary # Enforce continuity

                    # Contribution to enforce conservation of P
                    F[ptr_fr + 1] -= p_t

                    factor = np.sqrt(ws/(2*v*v*h*b))
                    w_b = state[ptr + NFIELD*(i + 1) + W_IDX]
                    p_b = state[ptr + NFIELD*(i + 1) + P_IDX]
                    F[ptr + NFIELD*i + P_IDX] = w_t - w_b - factor*(p_t - p_b)

                elif i == npoints - 1:
                    w_boundary = x[ptr_to  + W_IDX]
                    F[ptr + NFIELD*i + W_IDX] = w_t - w_boundary # Enforce continuity
                    
                    # Contribution to enforce conservation of P
                    F[ptr_to] += p_t

                    factor = np.sqrt(ws/(2*v*v*h*b))
                    w_a = state[ptr + NFIELD*(i - 1) + W_IDX]
                    p_a = state[ptr + NFIELD*(i - 1) + P_IDX]
                    F[ptr + NFIELD*i + P_IDX] = w_t - w_a + factor*(p_t - p_a)

                else:
                    p_b = state[ptr + NFIELD*(i - 1) + P_IDX]
                    w_b = state[ptr + NFIELD*(i - 1) + W_IDX]
                    p_f = state[ptr + NFIELD*(i + 1) + P_IDX]
                    w_f = state[ptr + NFIELD*(i + 1) + W_IDX]

                    dP = (p_f - p_b)/(2*dx)
                    dW = (w_f - w_b)/(2*dx)

                    w_hat = 0.5*(w_b + w_f)
                    p_hat = 0.5*(p_b + p_f)

                    F[ptr + NFIELD*i + P_IDX] = w_t - (w_hat + dt*(ws/(2*h))*(pg - dP))
                    F[ptr + NFIELD*i + W_IDX] = p_t - (p_hat - dt*(v**2)*b*dW)
    
    for i in range(len(bus_idx)):
        ptr = bus_ptr[i]
        #print("AFTER. Bus {0}. F[ptr]={1}. F[ptr + 1]={2}".format(i, F[ptr], F[ptr + 1]))


def residual_lw(F, x, state, net, dt, dx, boundary_type="characteristic"):

    F.fill(0.0)
    residual_lw_imp(F, x, state, dt, dx, net.voltage, net.NFIELD, net.bus_idx,
            net.bus_type, net.bus_pg, net.bus_vmag, net.bus_ptr,
            net.br_idx, net.br_ptr, net.br_fr, net.br_to, net.br_points,
            net.br_ws, net.br_h, net.br_b, net.br_v, net.br_pg, net.br_lon)

def residual_lw_root(x, state, net, dt, dx):
    F = np.zeros(len(x))
    residual_lw_imp(F, x, state, dt, dx, net.voltage, net.NFIELD, net.bus_idx,
            net.bus_type, net.bus_pg, net.bus_vmag, net.bus_ptr,
            net.br_idx, net.br_ptr, net.br_fr, net.br_to, net.br_points,
            net.br_ws, net.br_h, net.br_b, net.br_v, net.br_pg, net.br_lon)
    return F

@jit(nopython=True, cache=True)
def jacobian_lax_imp(J, x, state, dt, dx, vmode, NFIELD, bus_idx, bus_type, bus_pg, bus_ptr,
        br_idx, br_ptr, br_fr, br_to, br_points,
        br_ws, br_h, br_b, br_v, br_pg, br_lon):
    
    for i in range(len(bus_idx)):
        ptr = bus_ptr[i]
        J[ptr, ptr + P_IDX] = 1
        J[ptr + 1, ptr + P_IDX] = -1
        if vmode:
            J[ptr + 2, ptr + V_IDX] = 1



    for br in range(len(br_idx)):
        ptr = br_ptr[br]
        ptr_fr = bus_ptr[br_fr[br]]
        ptr_to = bus_ptr[br_to[br]]
        npoints = br_points[br]
        
        ws = br_ws[br]
        h = br_h[br]
        b = br_b[br]
        v = br_v[br]
        pg = br_pg[br]
        lon = br_lon[br]

        for i in range(npoints):
            if vmode:
                v = state[ptr + NFIELD*i + V_IDX]

            p_t_idx = ptr + NFIELD*i + P_IDX
            w_t_idx = ptr + NFIELD*i + W_IDX
            v_t_idx = ptr + NFIELD*i + V_IDX

            if i == 0:
                w_boundary_idx = ptr_fr + W_IDX # boundary frequency at time t + 1
                J[ptr + NFIELD*i + W_IDX, w_t_idx] = 1.0
                J[ptr + NFIELD*i + W_IDX, w_boundary_idx] = -1.0
               
                if vmode:
                    v_boundary_idx = ptr_fr + V_IDX # boundary voltage at time t + 1
                    J[ptr + NFIELD*i + V_IDX, v_t_idx] = 1.0
                    J[ptr + NFIELD*i + V_IDX, v_boundary_idx] = -1.0

                # Contribution to enforce conservation of P
                J[ptr_fr + 1, p_t_idx] = -1.0

                factor = np.sqrt(ws/(2*v*v*h*b))
                J[ptr + NFIELD*i + P_IDX, w_t_idx] = 1.0
                J[ptr + NFIELD*i + P_IDX, p_t_idx] = -factor*1.0

            elif i == npoints - 1:
                w_boundary_idx = ptr_to  + W_IDX
                J[ptr + NFIELD*i + W_IDX, w_t_idx] = 1.0
                J[ptr + NFIELD*i + W_IDX, w_boundary_idx] = -1.0
               
                if vmode:
                    v_boundary_idx = ptr_to + V_IDX # boundary voltage at time t + 1
                    J[ptr + NFIELD*i + V_IDX, v_t_idx] = 1.0
                    J[ptr + NFIELD*i + V_IDX, v_boundary_idx] = -1.0

                # Contribution to enforce conservation of P
                J[ptr_to, p_t_idx] = 1.0

                factor = np.sqrt(ws/(2*v*v*h*b))
                J[ptr + NFIELD*i + P_IDX, w_t_idx] = 1.0
                J[ptr + NFIELD*i + P_IDX, p_t_idx] = factor*1.0

            else:

                J[ptr + NFIELD*i + P_IDX, w_t_idx] = 1.0
                J[ptr + NFIELD*i + W_IDX, p_t_idx] = 1.0

                if vmode:
                    v_t_f_idx = ptr + NFIELD*(i - 1) + V_IDX
                    v_t_b_idx = ptr + NFIELD*(i + 1) + V_IDX
                    J[ptr + NFIELD*i + V_IDX, v_t_idx] = -2.0
                    J[ptr + NFIELD*i + V_IDX, v_t_f_idx] = 1.0
                    J[ptr + NFIELD*i + V_IDX, v_t_b_idx] = 1.0


def jacobian_lax(x, state, net, dt, dx, boundary_type="characteristic"):

    dim = x.size
    J = np.zeros((dim, dim))
    
    jacobian_lax_imp(J, x, state, dt, dx, net.voltage, net.NFIELD, net.bus_idx, net.bus_type, net.bus_pg, net.bus_ptr,
            net.br_idx, net.br_ptr, net.br_fr, net.br_to, net.br_points,
            net.br_ws, net.br_h, net.br_b, net.br_v, net.br_pg, net.br_lon)

    return csr_matrix(J)

def newton(F, x0, state, net, J, dt, dx, boundary_type="characteristic", itmax=20, eps=1e-8):

    iters = 0
    x = x0
    ier = 1

    residual_lw(F, x, state, net, dt, dx)
    Fnorm = np.linalg.norm(F)
    if VERBOSE: print("\t(N-R) Iteration %d. Residual norm: %g." % (iters, Fnorm))

    while Fnorm > eps and iters < itmax:

        xdelta = spsolve(J, F)
        x = x - xdelta

        residual_lw(F, x, state, net, dt, dx)
        Fnorm = np.linalg.norm(F)
        iters += 1
        if VERBOSE: print("\t(N-R) Iteration %d. Residual norm: %g." % (iters, Fnorm))

    if iters >= itmax:
        ier = -1

    return x, ier

def store_trajectories(net, state, traj_w, traj_p, idx):

    for i in range(net.nbus):
        bus = net.buses[i]
        ptr = net.get_busptr(bus.idx)
        traj_w[i, idx] = state[ptr + W_IDX]
        traj_p[i, idx] = state[ptr + P_IDX]


def integrate_system(net, int_class, nsteps, dt, dx, bus_fault=1,
        record_branch=0, ton=0.1, toff=0.2):

    print("CFL condition: {0}".format(dt/dx))

    print("Recording branch " + str(record_branch))
    print("Faulted bus " + str(bus_fault))

    # initialize
    state = np.zeros(net.dof)
    
    state, info, ier, msg = fsolve(residual_finit, state, args=(net,), full_output=True)
    
    widx = net.get_branch_field(record_branch, 0)
    pidx = net.get_branch_field(record_branch, 1)

    if net.voltage:
        print("voltage-dependent model")
    else:
        print("constant-voltage model")

    if net.voltage:
        vidx = net.get_branch_field(record_branch, 2)

    points = net.dof // 2

    # For each slack bus, set up the generated power as a variable.
    for bus in net.buses:
        if bus.type == TYPE_SLACKBUS:
            # obtain bus pointer and generator power
            ptr = net.get_busptr(bus.idx)
            bus.pg = state[ptr + P_IDX]
            net.bus_pg[bus.idx] = bus.pg
            state[ptr + P_IDX] = 0

            p_in = 0.0
            p_out = 0.0

            for idx_branch in bus.branches_fr:
                ptr_b = net.get_branchptr(idx_branch)
                p_out -= state[ptr_b + P_IDX]
            for idx_branch in bus.branches_to:
                pp_ptr = net.get_branch_field(idx_branch, 1)
                ptr_b = pp_ptr[-1]
                p_in += state[ptr_b]

            state[ptr + P_IDX] = p_out
            net.bus_pg[bus.idx] = -p_out - p_in

    trajectory_w = np.zeros((len(widx), nsteps + 1))
    trajectory_p = np.zeros((len(pidx), nsteps + 1))
    trajectory_v = np.zeros((len(pidx), nsteps + 1))

    trajectory_w[:,0] = state[widx]
    trajectory_p[:,0] = state[pidx]
    if net.voltage:
        trajectory_v[:,0] = state[vidx]
    else:
        trajectory_v[:,0] = 1.05

    # compute fault time
    step_on = floor(ton/dt)
    step_off = floor(toff/dt)

    # the jacobian is actually constant. we compute it only once
    if FD_JACOBIAN:
        # compute Jacobian using finite differences.
        rhs_fun = lambda x: residual_lw_root(x, state, net, dt, dx)
        nd_jac = nd.Jacobian(rhs_fun)
        Jac = csr_matrix(nd_jac(state))
        #print(Jac)
        #AA = Jac.todense() - jacobian_lax(state, state, net, dt, dx).todense()
        #print("error: {0}".format(np.linalg.norm(AA)))
    else:
        Jac = jacobian_lax(state, state, net, dt, dx)

    F = np.zeros_like(state)
    PDELTA = 1.1

    for i in range(nsteps):
        if VERBOSE: print("Time: %f sec. Step: %i." % (i*dt, i))
        if i == step_on:
            if VERBOSE: print("Applying FAULT")
            net.buses[bus_fault].pg = PDELTA*net.buses[bus_fault].pg
            net.bus_pg[bus_fault] = PDELTA*net.bus_pg[bus_fault]
        if i == step_off:
            if VERBOSE: print("Removing FAULT")
            net.buses[bus_fault].pg = (1.0/PDELTA)*net.buses[bus_fault].pg
            net.bus_pg[bus_fault] = (1.0/PDELTA)*net.bus_pg[bus_fault]
        
        x0 = state.copy()
        
        x0, ier = newton(F, x0, state, net, Jac, dt, dx)
        if ier != 1:
            # Something went wrong. Lets print the Jacobian and its properties.
            raise Exception("Nonlinear solver did not converge.")
        else:
            state = x0

        trajectory_w[:, i + 1] = state[widx]
        trajectory_p[:, i + 1] = state[pidx]
        if net.voltage:
            trajectory_v[:, i + 1] = state[vidx]
        else:
            trajectory_v[:, i + 1] = 1.05

    return trajectory_w, trajectory_p, trajectory_v

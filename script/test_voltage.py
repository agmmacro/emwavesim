# Run wave cases
from wave import Network, integrate_system, load_matpower
import matplotlib.pyplot as plt
import numpy as np

def runwave1():
    # parameters
    ws = 1.0
    h = 1.5
    pg = 0.0
    v = 1.0
    b = 400.0
    lon = 10.0

    # boundaries
    pg_gen = 1.0
    pg_load = -0.5

    # discretization
    nsteps = 2000
    dt = 0.001
    dx = 0.2

    # create system
    net = Network(voltage=False)
    net.add_branch(ws, h, pg, v, b, lon, 0, 1)
    net.add_bus(0, 1.1, pg_gen, "slack")
    net.add_bus(1, 1.0, pg_load, "load")
    net.assemble(dx)
    w_fd, p_fd, v_fd = integrate_system(net, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx)
    
    net2 = Network(voltage=True)
    net2.add_branch(ws, h, pg, v, b, lon, 0, 1)
    net2.add_bus(0, 1.1, pg_gen, "slack")
    net2.add_bus(1, 1.0, pg_load, "load")
    net2.assemble(dx)
    w_fd2, p_fd2, v_fd2 = integrate_system(net2, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx)
    time = np.linspace(0.0, dt*nsteps, nsteps + 1)

    # find frequency nadir
    min1 = 1e9
    min2 = 1e9
    min1_time = 0
    min2_time = 0

    for i in range(w_fd.shape[0]):
        idx1 = np.argmin(w_fd[i,:])
        idx2 = np.argmin(w_fd2[i,:])
        if w_fd[i,idx1] < min1:
            min1 = w_fd[i,idx1]
            min1_time = time[idx1]
        if w_fd2[i,idx2] < min2:
            min2 = w_fd[i,idx2]
            min2_time = time[idx2]

    fig, axs = plt.subplots(2, 1, sharex=True, sharey=True)
    for i in range(w_fd.shape[0]):
        axs[0].plot(time, v_fd[i,:], color='black')
        axs[0].set_title("Classical")
        axs[0].set_xlabel("vmag")
        axs[1].plot(time, v_fd2[i,:], color='black')
        axs[1].set_title("Voltage-dependence")
        axs[1].set_xlabel("vmag")
        axs[1].set_ylabel("Time (s)")

    plt.legend()
    plt.show()
    
    fig, axs = plt.subplots(2, 1, sharex=True, sharey=True)
    for i in range(w_fd.shape[0]):
        axs[0].plot(time, w_fd[i,:], color='black')
        axs[0].set_title("Classical")
        axs[0].set_xlabel("$\omega$")
        axs[1].plot(time, w_fd2[i,:], color='black')
        axs[1].set_title("Voltage-dependence")
        axs[1].set_xlabel("$\omega$")
        axs[1].set_ylabel("Time (s)")
    axs[0].axvline(x = min1_time, color = 'red', label = "Freq. nadir: " + str(min1_time))
    axs[1].axvline(x = min1_time, color = 'red', label = "Freq. nadir: " + str(min2_time))

    axs[0].legend()
    axs[1].legend()
    plt.show()
    
    min1 = 1e9
    min2 = 1e9
    min1_time = 0
    min2_time = 0

    for i in range(w_fd.shape[0]):
        idx1 = np.argmin(p_fd[i,:])
        idx2 = np.argmin(p_fd2[i,:])
        if p_fd[i,idx1] < min1:
            min1 = p_fd[i,idx1]
            min1_time = time[idx1]
        if p_fd2[i,idx2] < min2:
            min2 = p_fd[i,idx2]
            min2_time = time[idx2]
    
    fig, axs = plt.subplots(2, 1, sharex=True)
    for i in range(w_fd.shape[0]):
        axs[0].plot(time, p_fd[i,:], color='black')
        axs[0].set_title("Classical")
        axs[0].set_xlabel("$p$")
        axs[1].plot(time, p_fd2[i,:], color='black')
        axs[1].set_title("Voltage-dependence")
        axs[1].set_xlabel("$p$")
        axs[1].set_ylabel("Time (s)")
    axs[0].axvline(x = min1_time, color = 'red', label = "Power nadir: " + str(min1_time))
    axs[1].axvline(x = min2_time, color = 'red', label = "Power nadir: " + str(min2_time))
    axs[0].legend()
    axs[1].legend()
    plt.show()

    return w_fd

def runmat():
    nsteps = 2000
    dt = 0.001
    dx = 0.2
    net = load_matpower("data/case9.mat")
    net.assemble(dx)
    w_fd, p_fd, v_fd = integrate_system(net, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx, record_branch=1)

    np.save('w_fd3.npy', w_fd)
    np.save('p_fd3.npy', p_fd)
    np.save('v_fd3.npy', v_fd)


if __name__ == "__main__":

    aa = runwave1()
    #runmat()

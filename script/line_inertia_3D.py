# Run wave cases
from wave import Network, integrate_system, load_matpower
import matplotlib.pyplot as plt
import numpy as np
from math import floor

def plot_branch(sol):

    w_fd = sol["w_fd"]
    p_fd = sol["p_fd"]
    v_fd = sol["v_fd"]
    time = sol["time"]

    fig, axs = plt.subplots(2, 1)
    for i in range(len(w_fd)):
        axs[0].plot(time, w_fd[i, :], color='grey')
    axs[0].grid(True)
    axs[0].set_ylabel("$\Delta w$ (p.u)")
    for i in range(len(p_fd)):
        axs[1].plot(time, p_fd[i, :], color='grey')
    axs[1].grid(True)
    axs[1].set_ylabel("$P$ (p.u)")
    axs[1].set_xlabel("Time (s)")
    plt.show()

def plot_compare(sol1, sol2, field="w_fd", title1="caseA",
        title2="caseB"):

    assert (field in ["w_fd", "p_fd", "v_fd"])

    qoi1 = sol1[field]
    qoi2 = sol2[field]

    tm1 = sol1["time"]
    tm2 = sol2["time"]
    
    fig, axs = plt.subplots(2, 1)
    for i in range(len(qoi1)):
        axs[0].plot(tm1, qoi1[i, :], color='grey')
    axs[0].set_title(title1)
    axs[0].grid(True)
    for i in range(len(qoi2)):
        axs[1].plot(tm2, qoi2[i, :], color='grey')
    axs[1].grid(True)
    axs[1].set_title(title2)
    axs[1].set_xlabel("Time (s)")
    plt.show()

def case_oneline(dx=0.2, voltage=False, b=400.0, h=1.5):
    
    # parameters
    ws = 1.0
    pg = 0.0
    v = 1.0
    lon = 10.0

    # boundaries
    pg_gen = 1.0
    pg_load = -1.5

    # create system
    net = Network(voltage=voltage)
    net.add_branch(ws, h, pg, v, b, lon, 0, 1)

    net.add_bus(0, 1.1, pg_gen, "slack")
    net.add_bus(1, 1.0, pg_load, "load")
    net.assemble(dx)

    return net
    

def runwave(net, record_branch=0, bus_fault=0, dt=1.0/1200):
    
    # discretization
    tend = 2.0
    nsteps = floor(tend/dt)
    ton = 0.1
    toff = 0.2

    w_fd, p_fd, v_fd = integrate_system(
            net,
            int_class='lax',
            nsteps=nsteps,
            dt=dt,
            dx=net.dx,
            record_branch=record_branch,
            bus_fault=bus_fault,
            ton=ton,
            toff=toff)
 
    time = np.linspace(0.0, dt*nsteps, nsteps + 1)

    sol = {}
    sol["w_fd"] = w_fd
    sol["p_fd"] = p_fd
    sol["v_fd"] = v_fd
    sol["time"] = time

    return sol

dx = 0.1
dt = 0.001

rbranch = 0
bfault = 1

net1 = case_oneline(dx=dx, h=1.5)
net2 = case_oneline(dx=dx, h=15.0)

#net1 = load_matpower("data/case9.mat")
#net1.assemble(dx1)
#net2 = load_matpower("data/case9.mat")
#net2.assemble(dx2)

sol1 = runwave(net1, record_branch=rbranch, bus_fault=bfault, dt=dt)
sol2 = runwave(net2, record_branch=rbranch, bus_fault=bfault, dt=dt)

plot_compare(sol1, sol2, field="p_fd", title1="system A".format(str(dx)),
        title2 = "system B".format(str(dx)))
plot_compare(sol1, sol2, field="w_fd", title1="system A".format(str(dx)),
        title2 = "system B".format(str(dx)))
plot_compare(sol1, sol2, field="v_fd", title1="system A".format(str(dx)),
        title2 = "system B".format(str(dx)))


fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.add_subplot(1, 2, 1, projection='3d')
time = sol1["time"]

# Data for a three-dimensional line
for i in range(len(sol1["w_fd"])):
    zline = 1 + sol1["w_fd"][i, :]
    yline = i*np.ones(len(zline))
    xline = time
    ax.plot3D(xline, yline, zline, 'gray')
ax.set_xlabel('\n\nTime (sec)', size=20)
ax.set_zlabel('\n\nFrequency ($\omega$) in p.u',size=20)
ax.set_ylabel('\n\nLine discretization index', size=20)

ax = fig.add_subplot(1, 2, 2, projection='3d')
time = sol2["time"]
# Data for a three-dimensional line
for i in range(len(sol2["w_fd"])):
    zline = 1 + sol2["w_fd"][i, :]
    yline = i*np.ones(len(zline))
    xline = time
    ax.plot3D(xline, yline, zline, 'gray')
ax.set_xlabel('\n\nTime (sec)', size=20)
ax.set_zlabel('\n\nFrequency ($\omega$) in p.u',size=20)
ax.set_ylabel('\n\nLine discretization index', size=20)
plt.show()

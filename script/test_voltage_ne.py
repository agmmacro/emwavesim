# Run wave cases
from wave import Network, integrate_system, load_matpower
import matplotlib.pyplot as plt
import numpy as np

# See MATPOWER file for numbering.
# GENERATOR 9 - BUS 38 - IBUS 37
# LINE 26-29 - ILINE # 43
# LINE 13-14 - ILINE # 22

def runmat():
    nsteps = 2000
    dt = 1.0/120.0
    dx = 0.5

    net = load_matpower("data/case39.mat")
    #net = load_matpower("data/case9.mat")
    net.assemble(dx)
    
    #w_fd, p_fd, v_fd = integrate_system(net, int_class='lax',
    #nsteps=nsteps, dt=dt, dx=dx, record_branch=0, bus_fault=1)
    
    w_fd, p_fd, v_fd = integrate_system(net, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx, record_branch=43, bus_fault=37)

    np.save('ne_w_fd.npy', w_fd)
    np.save('ne_p_fd.npy', p_fd)
    np.save('ne_v_fd.npy', v_fd)

    w_fd, p_fd, v_fd = integrate_system(net, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx, record_branch=22, bus_fault=37)

    np.save('ne_w_fd2.npy', w_fd)
    np.save('ne_p_fd2.npy', p_fd)
    np.save('ne_v_fd2.npy', v_fd)

if __name__ == "__main__":
    runmat()

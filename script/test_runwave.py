# Run wave cases
from wave import Network, integrate_system, load_matpower
import matplotlib.pyplot as plt


def runwave1():
    # parameters
    ws = 1.0
    h = 1.5
    pg = 0.0
    v = 1.0
    b = 400.0
    lon = 10.0

    # boundaries
    pg_gen = 1.0
    pg_load = -0.5

    # discretization
    nsteps = 2000
    dt = 0.001
    dx = 0.2

    # create system
    net = Network()
    net.add_branch(ws, h, pg, v, b, lon, 0, 1)
    net.add_branch(ws, h, pg, v, b, 0.5*lon, 1, 2)
    net.add_branch(ws, h, pg, v, b, 2*lon, 1, 3)
    net.add_branch(ws, h, pg, v, b, 2*lon, 1, 4)

    net.add_bus(0, pg_gen, "slack")
    net.add_bus(1, -0.001, "load")
    net.add_bus(2, -0.001, "load")
    net.add_bus(3, pg_load, "load")
    net.add_bus(4, pg_load, "load")
    net.assemble(dx)

    
    w_fd, p_fd, map_matrix = integrate_system(net, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx)
    
    fig = plt.figure(figsize=(15,6))
    for i in range(net.nbus):
        plt.plot(w_fd[i,:], label=("Bus " + str(i)))
    plt.legend()
    plt.show()

def runmat():
    nsteps = 2000
    dt = 0.001
    dx = 0.2
    net = load_matpower("data/case9.mat")
    net.assemble(dx)
    w_fd, p_fd, map_matrix = integrate_system(net, int_class='lax',
    nsteps=nsteps, dt=dt, dx=dx)

    fig = plt.figure(figsize=(15,6))
    for i in range(net.nbus):
        plt.plot(w_fd[i,:], label=("Bus " + str(i)))
    plt.legend()
    plt.show()

if __name__ == "__main__":

    runwave1()
    #runmat()

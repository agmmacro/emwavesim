#include "wave.h"

// Implementation of wave
PetscErrorCode InitFunction(SNES snes,Vec X, Vec F,void *appctx)
{
    PetscErrorCode ierr;
    DM             networkdm;
    Vec            localX,localF;
    PetscInt       e,v,vStart,vEnd,eStart, eEnd;
    PetscInt       lofst,lofst_to,lofst_fr;
    PetscBool      ghost;
    Branch         *branch;
    Bus            *bus;
    PetscScalar    *xarr;
    PetscScalar    *farr, *busf, *branchf;
    PetscScalar    *busx, *branchx;
    const PetscInt *cone;
    PetscInt       i, nend;
    
    PetscFunctionBegin;
    ierr = SNESGetDM(snes,&networkdm);CHKERRQ(ierr);
    ierr = DMGetLocalVector(networkdm,&localX);CHKERRQ(ierr);
    ierr = DMGetLocalVector(networkdm,&localF);CHKERRQ(ierr);
    ierr = VecSet(F,0.0);CHKERRQ(ierr);
    ierr = VecSet(localF,0.0);CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);

    ierr = VecGetArray(localX,&xarr);CHKERRQ(ierr);
    ierr = VecGetArray(localF,&farr);CHKERRQ(ierr);

    // Bus equations
    ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
    for (v = vStart; v < vEnd; v++) {
        ierr = DMNetworkIsGhostVertex(networkdm,v,&ghost);CHKERRQ(ierr);
        if (!ghost) {
            ierr = DMNetworkGetComponent(networkdm,v,0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
            ierr = DMNetworkGetLocalVecOffset(networkdm,v,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

            busf = (PetscScalar*)(farr + lofst);
            busx = (PetscScalar*)(xarr + lofst);

            if (bus->type == BUS_TYPE_SLACK) {
                busf[0] = busx[W_IDX]; // set w_slack to 0
                busf[1] = busx[P_IDX];
            } else {
                busf[0] = busx[P_IDX] + bus->pinj;
                busf[1] = -busx[P_IDX];
            }
        }
    }

    // Branch equations
    
    PetscScalar dx = 0.0;

    ierr = DMNetworkGetEdgeRange(networkdm,&eStart,&eEnd);CHKERRQ(ierr);
    for (e = eStart; e < eEnd; e++) {
        ierr = DMNetworkGetComponent(networkdm,e,0,NULL,(void**)&branch,NULL);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,e,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

        ierr = DMNetworkGetConnectedVertices(networkdm,e,&cone);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[0],ALL_COMPONENTS,&lofst_fr);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[1],ALL_COMPONENTS,&lofst_to);CHKERRQ(ierr);

        branchx    = (PetscScalar*)(xarr + lofst);
        branchf    = (PetscScalar*)(farr + lofst);

        nend = branch->npoints;
        dx = branch->length / (nend - 1);

        /* First fill up the inner points of the branch */
        for (i = 0; i < nend - 1; i++) {
            branchf[2*i] = branchx[2*(i + 1) + W_IDX] - branchx[2*i + W_IDX];
            branchf[2*i + 1] = branchx[2*(i + 1) + P_IDX] - branchx[2*i + P_IDX];
        }

        /* Evaluate upstream boundary (from bus)*/
        busf = (PetscScalar*)(farr + lofst_fr);
        busx = (PetscScalar*)(xarr + lofst_fr);

        branchf[2*(nend - 1)] = branchx[W_IDX] - busx[W_IDX];

        ierr = DMNetworkGetComponent(networkdm,cone[0],0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
        if (bus->type == BUS_TYPE_SLACK) {
            busf[1] -= branchx[P_IDX]; // conservation
        } else {
            busf[1] -= branchx[P_IDX]; // conservation            
        }
        
        /* Evaluate downstream  boundary (to bus)*/
        busf = (PetscScalar*)(farr + lofst_to);
        busx = (PetscScalar*)(xarr + lofst_to);

        branchf[2*(nend - 1) + 1] = branchx[2*(nend - 1) + W_IDX] - busx[W_IDX];

        ierr = DMNetworkGetComponent(networkdm,cone[1],0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
        if (bus->type == BUS_TYPE_SLACK) {
            busf[1] -= branchx[2*(nend - 1) + P_IDX]; //conservation
        } else {
            busf[0] += branchx[2*(nend - 1) + P_IDX]; //conservation         
        }
    }

    ierr = DMRestoreLocalVector(networkdm,&localX);CHKERRQ(ierr);
    ierr = DMLocalToGlobalBegin(networkdm,localF,ADD_VALUES,F);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(networkdm,localF,ADD_VALUES,F);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(networkdm,&localF);CHKERRQ(ierr);
    PetscFunctionReturn(0);
}

PetscErrorCode InitJacobian(SNES snes,Vec X, Mat J,Mat Jpre,void *appctx)
{
    PetscErrorCode ierr;
    DM             networkdm;
    Vec            localX;
    PetscInt       e,v,vStart,vEnd,eStart,eEnd;
    PetscInt       lofst,lofst_to,lofst_fr;
    PetscBool      ghost;
    Branch         *branch;
    Bus            *bus;
    const PetscInt *cone;
    PetscInt       i, nend;

    PetscInt       row[1], col[2];
    PetscScalar    values[2];

    PetscFunctionBegin;
    ierr = MatZeroEntries(J);CHKERRQ(ierr);

    ierr = SNESGetDM(snes,&networkdm);CHKERRQ(ierr);
    ierr = DMGetLocalVector(networkdm,&localX);CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);

    // Bus equations
    ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
    for (v = vStart; v < vEnd; v++) {
        ierr = DMNetworkIsGhostVertex(networkdm,v,&ghost);CHKERRQ(ierr);
        if (!ghost) {
            ierr = DMNetworkGetComponent(networkdm,v,0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
            ierr = DMNetworkGetLocalVecOffset(networkdm,v,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

            if (bus->type == BUS_TYPE_SLACK) {
                row[0] = lofst;
                col[0] = lofst + W_IDX;
                values[0] = 1.0;
                ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
                
                row[0] = lofst + 1;
                col[0] = lofst + P_IDX;
                values[0] = 1.0;
                ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
            } else {
                row[0] = lofst;
                col[0] = lofst + P_IDX;
                values[0] = 1.0;
                ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
                
                row[0] = lofst + 1;
                col[0] = lofst + P_IDX;
                values[0] = -1.0;
                ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
            }
        }
    }
    
    // Branch equations
    ierr = DMNetworkGetEdgeRange(networkdm,&eStart,&eEnd);CHKERRQ(ierr);
    for (e = eStart; e < eEnd; e++) {
        ierr = DMNetworkGetComponent(networkdm,e,0,NULL,(void**)&branch,NULL);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,e,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

        ierr = DMNetworkGetConnectedVertices(networkdm,e,&cone);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[0],ALL_COMPONENTS,&lofst_fr);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[1],ALL_COMPONENTS,&lofst_to);CHKERRQ(ierr);

        nend = branch->npoints;

        /* First fill up the inner points of the branch */
        for (i = 0; i < nend - 1; i++) {

            /* Power balance */
            // branchf[2*i] = branchx[2*(i + 1)] - branchx[2*i];
            row[0] = lofst + 2*i;
            col[0] = lofst + 2*i + W_IDX;
            col[1] = lofst + 2*(i + 1) + W_IDX;
            values[0] = -1.0;
            values[1] = 1.0;
            ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);

            /* Frequency continuity */
            // branchf[2*i + 1] = branchx[2*(i + 1) + 1] - branchx[2*i + 1];
            row[0] = lofst + 2*i + 1;
            col[0] = lofst + 2*i + P_IDX;
            col[1] = lofst + 2*(i + 1) + P_IDX;
            values[0] = -1.0;
            values[1] = 1.0;
            ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);
        }

        /* Evaluate upstream boundary (fr bus) */
        //branchf[2*(nend - 1)] = branchx[1] - busx[1];
        row[0] = lofst + 2*(nend - 1);
        col[0] = lofst + W_IDX;
        col[1] = lofst_fr + W_IDX;
        values[0] = 1.0;
        values[1] = -1.0;
        ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);
        
        //busf[1] -= branchx[0]; // conservation
        row[0] = lofst_fr + 1;
        col[0] = lofst + P_IDX;
        values[0] = -1.0;
        ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
        
        /* Evaluate downstream  boundary (to bus)*/
        // branchf[2*(nend - 1) + 1] = branchx[2*(nend - 1) + 1] - busx[1];
        row[0] = lofst + 2*(nend - 1) + 1;
        col[0] = lofst + 2*(nend - 1) + W_IDX;
        col[1] = lofst_to + W_IDX;
        values[0] = 1.0;
        values[1] = -1.0;
        ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);
        
        ierr = DMNetworkGetComponent(networkdm,cone[1],0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
        // if (bus->type == BUS_TYPE_SLACK) {
        //     busf[1] -= branchx[2*(nend - 1) + P_IDX]; //conservation
        // } else {
        //     busf[0] += branchx[2*(nend - 1) + P_IDX]; //conservation         
        // }
        if (bus->type == BUS_TYPE_SLACK) {
            row[0] = lofst_to + 1;
            col[0] = lofst + 2*(nend - 1) + P_IDX;
            values[0] = -1.0;
        } else {
            row[0] = lofst_to;
            col[0] = lofst + 2*(nend - 1) + P_IDX;
            values[0] = 1.0;            
        }
        ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
    }

    ierr = DMRestoreLocalVector(networkdm,&localX);CHKERRQ(ierr);
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    PetscFunctionReturn(0);
}


PetscErrorCode PostProcessInitialization(DM networkdm, Vec X)
{
    PetscErrorCode ierr;
    Vec            localX;
    PetscInt       e,v,vStart,vEnd,eStart, eEnd;
    PetscInt       lofst,lofst_fr;
    PetscBool      ghost;
    Branch         *branch;
    Bus            *bus;
    PetscScalar *xarr;
    PetscScalar          *busx, *branchx;
    const PetscInt *cone;

    PetscFunctionBegin;
    ierr = DMGetLocalVector(networkdm,&localX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);
    ierr = VecGetArray(localX,&xarr);CHKERRQ(ierr);

    // Bus equations
    ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
    for (v = vStart; v < vEnd; v++) {
        ierr = DMNetworkIsGhostVertex(networkdm,v,&ghost);CHKERRQ(ierr);
        if (!ghost) {
            ierr = DMNetworkGetComponent(networkdm,v,0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
            ierr = DMNetworkGetLocalVecOffset(networkdm,v,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

            busx = (PetscScalar*)(xarr + lofst);
            printf("lofst: %d.\n", lofst);

            if (bus->type == BUS_TYPE_SLACK) {
                bus->pinj = busx[P_IDX];
                busx[P_IDX] = 0.0;
            }

        }
    }

    ierr = DMNetworkGetEdgeRange(networkdm,&eStart,&eEnd);CHKERRQ(ierr);
    for (e = eStart; e < eEnd; e++) {
        ierr = DMNetworkGetComponent(networkdm,e,0,NULL,(void**)&branch,NULL);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,e,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

        ierr = DMNetworkGetConnectedVertices(networkdm,e,&cone);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[0],ALL_COMPONENTS,&lofst_fr);CHKERRQ(ierr);

        branchx    = (PetscScalar*)(xarr + lofst);
        busx = (PetscScalar*)(xarr + lofst_fr);

        ierr = DMNetworkGetComponent(networkdm,cone[0],0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
        if (bus->type == BUS_TYPE_SLACK) {
            busx[P_IDX] -= branchx[P_IDX]; // conservation
        }
        
    }

    ierr = DMLocalToGlobalBegin(networkdm,localX,INSERT_VALUES,X);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(networkdm,localX,INSERT_VALUES,X);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(networkdm,&localX);CHKERRQ(ierr);
    PetscFunctionReturn(0);
}

PetscErrorCode SetFault(DM networkdm, PetscInt bus_id, PetscScalar percent)
{
    PetscErrorCode ierr;
    PetscInt       v,vStart,vEnd;
    PetscBool      ghost;
    Bus            *bus;

    PetscFunctionBegin;

    // Bus equations
    ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
    for (v = vStart; v < vEnd; v++) {
        ierr = DMNetworkIsGhostVertex(networkdm,v,&ghost);CHKERRQ(ierr);
        if (!ghost) {
            ierr = DMNetworkGetComponent(networkdm,v,0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
            if (bus->id == bus_id) {
                bus->pinj = percent*bus->pinj;
            }

        }
    }

    PetscFunctionReturn(0);
}


PetscErrorCode WAVEIFunction(TS ts,PetscReal t,Vec X,Vec Xdot,Vec F,void* ctx)
{
    PetscErrorCode ierr;
    DM             networkdm;
    Vec            localX,localXdot,localF, localXold;
    const PetscInt *cone;
    PetscInt       v,vStart,vEnd,e,eStart,eEnd;
    PetscInt       lofst,lofst_to,lofst_fr;
    PetscInt       nend;
    PetscBool      ghost;
    PetscScalar    *farr, *busf, *branchf;
    PetscScalar    *busx, *branchx;
    PetscScalar    *branchxold;
    PetscReal      dt;
    const PetscScalar *xarr,*xdotarr, *xoldarr;
    Branch         *branch;
    Bus            *bus;
    PetscInt       i;

    ierr = TSGetSolution(ts,&localXold);CHKERRQ(ierr);
    ierr = TSGetDM(ts,&networkdm);CHKERRQ(ierr);
    ierr = TSGetTimeStep(ts,&dt);CHKERRQ(ierr);
    ierr = DMGetLocalVector(networkdm,&localF);CHKERRQ(ierr);
    ierr = DMGetLocalVector(networkdm,&localX);CHKERRQ(ierr);
    ierr = DMGetLocalVector(networkdm,&localXdot);CHKERRQ(ierr);

    /* Set F and localF as zero */
    ierr = VecSet(F,0.0);CHKERRQ(ierr);
    ierr = VecSet(localF,0.0);CHKERRQ(ierr);

    /* Update ghost values of locaX and locaXdot */
    ierr = DMGlobalToLocalBegin(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(networkdm,X,INSERT_VALUES,localX);CHKERRQ(ierr);

    ierr = DMGlobalToLocalBegin(networkdm,Xdot,INSERT_VALUES,localXdot);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(networkdm,Xdot,INSERT_VALUES,localXdot);CHKERRQ(ierr);

    ierr = VecGetArrayRead(localX,&xarr);CHKERRQ(ierr);
    ierr = VecGetArrayRead(localXdot,&xdotarr);CHKERRQ(ierr);
    ierr = VecGetArrayRead(localXold,&xoldarr);CHKERRQ(ierr);
    ierr = VecGetArray(localF,&farr);CHKERRQ(ierr);

    ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
    for (v = vStart; v < vEnd; v++) {
        ierr = DMNetworkIsGhostVertex(networkdm,v,&ghost);CHKERRQ(ierr);
        if (!ghost) {
            ierr = DMNetworkGetComponent(networkdm,v,0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
            ierr = DMNetworkGetLocalVecOffset(networkdm,v,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

            busf = (PetscScalar*)(farr + lofst);
            busx = (PetscScalar*)(xarr + lofst);

            busf[0] = busx[P_IDX] + bus->pinj;
            busf[1] = -busx[P_IDX];
        
        }
    }


    // Branch equations
    PetscScalar dx = 0.0;
    PetscScalar NU = 1.0e+1, factor;
    PetscScalar p_b, w_b, p_f, w_f, w_t, p_t, p, w;
    PetscScalar dP, dW, Pxx, Wxx;
    PetscScalar ws, h, b, volt, pg;
    PetscScalar what, phat;

    ierr = DMNetworkGetEdgeRange(networkdm,&eStart,&eEnd);CHKERRQ(ierr);
    for (e = eStart; e < eEnd; e++) {
        ierr = DMNetworkGetComponent(networkdm,e,0,NULL,(void**)&branch,NULL);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,e,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

        ierr = DMNetworkGetConnectedVertices(networkdm,e,&cone);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[0],ALL_COMPONENTS,&lofst_fr);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[1],ALL_COMPONENTS,&lofst_to);CHKERRQ(ierr);

        branchx    = (PetscScalar*)(xarr + lofst);
        branchxold = (PetscScalar*)(xoldarr + lofst);
        branchf    = (PetscScalar*)(farr + lofst);

        nend = branch->npoints;
        dx = branch->length / (nend - 1);

        /* Branch parameters */
        ws = branch->w_0;
        h = branch->h;
        b = branch->b;
        volt = branch->voltage;
        pg = branch->pg;

        for (i = 1; i < nend - 1; i++) {
            p_b = branchxold[2*(i - 1) + P_IDX];
            w_b = branchxold[2*(i - 1) + W_IDX];
            p_f = branchxold[2*(i + 1) + P_IDX];
            w_f = branchxold[2*(i + 1) + W_IDX];
            p   = branchxold[2*i + P_IDX];
            w   = branchxold[2*i + W_IDX];

            w_t = branchx[2*i + W_IDX];
            p_t = branchx[2*i + P_IDX];

            dP = (p_f - p_b)/(2*dx);
            dW = (w_f - w_b)/(2*dx);

            what = 0.5*(w_b + w_f);
            phat = 0.5*(p_b + p_f);

            branchf[2*i] = w_t - (what + dt*(ws/(2*h))*(pg - dP));
            branchf[2*i + 1] = p_t - (phat - dt*(volt*volt)*b*dW);

        }

        /* Upstream boundary */
        busf = (PetscScalar*)(farr + lofst_fr);
        busx = (PetscScalar*)(xarr + lofst_fr);
        p_b = branchxold[2 + P_IDX];
        w_b = branchxold[2 + W_IDX];
        w_t = branchx[W_IDX];
        p_t = branchx[P_IDX];
        factor = PetscSqrtReal(ws/(2*volt*volt*h*b));

        busf[1] -= p_t; // conservation
        branchf[0] = w_t - busx[W_IDX]; //continuity
        branchf[1] = w_t - w_b - factor*(p_t - p_b); // characteristic

        /* Downstream boundary */
        busf = (PetscScalar*)(farr + lofst_to);
        busx = (PetscScalar*)(xarr + lofst_to);
        p_b = branchxold[2*(nend - 2) + P_IDX];
        w_b = branchxold[2*(nend - 2) + W_IDX];
        w_t = branchx[2*(nend - 1) + W_IDX];
        p_t = branchx[2*(nend - 1) + P_IDX];
        factor = PetscSqrtReal(ws/(2*volt*volt*h*b));

        busf[0] += p_t; // conservation
        branchf[2*(nend - 1)] = w_t - busx[W_IDX]; //continuity
        branchf[2*(nend - 1) + 1] = w_t - w_b + factor*(p_t - p_b); // characteristic
    }

    ierr = VecRestoreArrayRead(localX,&xarr);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(localXdot,&xdotarr);CHKERRQ(ierr);
    ierr = VecRestoreArray(localF,&farr);CHKERRQ(ierr);

    ierr = DMLocalToGlobalBegin(networkdm,localF,ADD_VALUES,F);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(networkdm,localF,ADD_VALUES,F);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(networkdm,&localF);CHKERRQ(ierr);

    PetscFunctionReturn(0);
}

PetscErrorCode WAVEIJacobian(TS ts,PetscReal t,Vec X,Vec X_t,PetscReal a,Mat J,Mat Jpre,void *appctx)
{
    PetscErrorCode ierr;
    DM             networkdm;
    PetscInt       e,v,vStart,vEnd,eStart,eEnd;
    PetscInt       lofst,lofst_to,lofst_fr;
    PetscBool      ghost;
    Branch         *branch;
    Bus            *bus;
    const PetscInt *cone;
    PetscInt       i, nend;

    PetscInt       row[1], col[2];
    PetscScalar    values[2];

    PetscFunctionBegin;
    ierr = MatZeroEntries(J);CHKERRQ(ierr);

    ierr = TSGetDM(ts,&networkdm);CHKERRQ(ierr);

    // Bus equations
    ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
    for (v = vStart; v < vEnd; v++) {
        ierr = DMNetworkIsGhostVertex(networkdm,v,&ghost);CHKERRQ(ierr);
        if (!ghost) {
            ierr = DMNetworkGetComponent(networkdm,v,0,NULL,(void**)&bus,NULL);CHKERRQ(ierr);
            ierr = DMNetworkGetLocalVecOffset(networkdm,v,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

            row[0] = lofst;
            col[0] = lofst + P_IDX;
            values[0] = 1.0;
            ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
            
            row[0] = lofst + 1;
            col[0] = lofst + P_IDX;
            values[0] = -1.0;
            ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
        }
    }
    
    // Branch equations

    PetscScalar ws, h, b, volt;
    PetscScalar factor;

    ierr = DMNetworkGetEdgeRange(networkdm,&eStart,&eEnd);CHKERRQ(ierr);
    for (e = eStart; e < eEnd; e++) {
        ierr = DMNetworkGetComponent(networkdm,e,0,NULL,(void**)&branch,NULL);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,e,ALL_COMPONENTS,&lofst);CHKERRQ(ierr);

        ierr = DMNetworkGetConnectedVertices(networkdm,e,&cone);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[0],ALL_COMPONENTS,&lofst_fr);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[1],ALL_COMPONENTS,&lofst_to);CHKERRQ(ierr);

        nend = branch->npoints;
        ws = branch->w_0;
        h = branch->h;
        b = branch->b;
        volt = branch->voltage;

        /* First fill up the inner points of the branch */
        for (i = 1; i < nend - 1; i++) {
            row[0] = lofst + 2*i;
            col[0] = lofst + 2*i + W_IDX;
            values[0] = 1.0;
            ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);

            row[0] = lofst + 2*i + 1;
            col[0] = lofst + 2*i + P_IDX;
            values[0] = 1.0;
            ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);
        }

        /* Upstream boundary */
        factor = PetscSqrtReal(ws/(2*volt*volt*h*b));

        row[0] = lofst_fr + 1;
        col[0] = lofst + P_IDX;
        values[0] = -1.0;
        ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);

        row[0] = lofst;
        col[0] = lofst + W_IDX;
        col[1] = lofst_fr + W_IDX;        
        values[0] = 1.0;
        values[1] = -1.0;
        ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);

        row[0] = lofst + 1;
        col[0] = lofst + W_IDX;
        col[1] = lofst + P_IDX;        
        values[0] = 1.0;
        values[1] = -factor;
        ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);

        /* Downstream boundary */
        factor = PetscSqrtReal(ws/(2*volt*volt*h*b));

        row[0] = lofst_to;
        col[0] = lofst + 2*(nend - 1) + P_IDX;
        values[0] = 1.0;
        ierr = MatSetValues(J,1,row,1,col,values,ADD_VALUES);CHKERRQ(ierr);

        row[0] = lofst + 2*(nend - 1);
        col[0] = lofst + 2*(nend - 1) + W_IDX;
        col[1] = lofst_to + W_IDX;        
        values[0] = 1.0;
        values[1] = -1.0;
        ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);

        row[0] = lofst + 2*(nend - 1) + 1;
        col[0] = lofst + 2*(nend - 1) + W_IDX;
        col[1] = lofst + 2*(nend - 1) + P_IDX;      
        values[0] = 1.0;
        values[1] = factor;
        ierr = MatSetValues(J,1,row,2,col,values,ADD_VALUES);CHKERRQ(ierr);

        ierr = DMNetworkGetConnectedVertices(networkdm,e,&cone);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[0],ALL_COMPONENTS,&lofst_fr);CHKERRQ(ierr);
        ierr = DMNetworkGetLocalVecOffset(networkdm,cone[1],ALL_COMPONENTS,&lofst_to);CHKERRQ(ierr);
    }

    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    PetscFunctionReturn(0);
}

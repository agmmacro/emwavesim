# Electromechanical Wave Simulator

This is a prototype of an electromechanical wave simulator for power systems. It uses a modified version of the one-dimensional homogeneization model of Semlyen-Thorp.

# Funding

This research is supported by the Advanced Grid Modeling Program (Department of Energy)

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

OBJECTS_WAVE = main.o wave.o PFReadData.o

wave: $(OBJECTS_WAVE)
	-${CLINKER} -o wave $(OBJECTS_WAVE) ${PETSC_TS_LIB}
	${RM} $(OBJECTS_WAVE)

static char help[] = "Electromechanical wave.";

#include "wave.h"

// FUNCTIONS
PetscErrorCode CreateNetwork(MPI_Comm comm, Network *network_ptr)
{
    PetscErrorCode ierr;
    PetscMPIInt    rank;

    Branch *branches;
    Bus *buses;

    PetscFunctionBegin;
    ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

    network_ptr->nbuses = 0;
    network_ptr->nbranches = 0;

    if (!rank) {
    // We create a network of one branch and two nodes: a generator and a load.

        network_ptr->nbuses = 2;
        network_ptr->nbranches = 1;

        ierr = PetscCalloc1(2*network_ptr->nbranches,&(network_ptr->edgelist));CHKERRQ(ierr);
        network_ptr->edgelist[0] = 0;
        network_ptr->edgelist[1] = 1;

        ierr = PetscCalloc2(2,&buses,1,&branches);CHKERRQ(ierr);

        // Create buses
        buses[0].id = 0;
        buses[0].type = BUS_TYPE_SLACK;
        buses[0].pinj = 1.0;
        buses[1].id = 1;
        buses[1].type = BUS_TYPE_PQ;
        buses[1].pinj = -1.5;

        // Create branches
        branches[0].id = 0;
        branches[0].length = 10.0;
        branches[0].b = 400.0;
        branches[0].voltage = 1.0;
        branches[0].h = 1.0;
        branches[0].w_0 = 1.0;
        branches[0].npoints = 6;
        branches[0].pg = 0.0;
    }

    network_ptr->buses = buses;
    network_ptr->branches = branches;

    return ierr;
}

PetscErrorCode CreateNetwork2(MPI_Comm comm, Network *network_ptr)
{
    PetscErrorCode ierr;
    PetscMPIInt    rank;

    Branch *branches;
    Bus *buses;

    PetscFunctionBegin;
    ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

    network_ptr->nbuses = 0;
    network_ptr->nbranches = 0;

    if (!rank) {
    // We create a network of one branch and two nodes: a generator and a load.

        network_ptr->nbuses = 3;
        network_ptr->nbranches = 2;

        ierr = PetscCalloc1(2*network_ptr->nbranches,&(network_ptr->edgelist));CHKERRQ(ierr);
        network_ptr->edgelist[0] = 0;
        network_ptr->edgelist[1] = 1;
        network_ptr->edgelist[2] = 1;
        network_ptr->edgelist[3] = 2;

        ierr = PetscCalloc2(3,&buses,2,&branches);CHKERRQ(ierr);

        // Create buses
        buses[0].id = 0;
        buses[0].type = BUS_TYPE_SLACK;
        buses[0].pinj = 2.0;
        buses[1].id = 1;
        buses[1].type = BUS_TYPE_PV;
        buses[1].pinj = 0.5;
        buses[2].id = 2;
        buses[2].type = BUS_TYPE_PQ;
        buses[2].pinj = -1.5;

        // Create branches
        branches[0].id = 0;
        branches[0].length = 5.0;
        branches[0].b = 400.0;
        branches[0].voltage = 1.0;
        branches[0].h = 1.0;
        branches[0].w_0 = 1.0;
        branches[0].npoints = 6;
        branches[0].pg = 0.0;

        branches[1].id = 1;
        branches[1].length = 5.0;
        branches[1].b = 400.0;
        branches[1].voltage = 1.0;
        branches[1].h = 1.0;
        branches[1].w_0 = 1.0;
        branches[1].npoints = 6;
        branches[1].pg = 0.0;
    }

    network_ptr->buses = buses;
    network_ptr->branches = branches;

    return ierr;
}

PetscErrorCode CreateFromData(MPI_Comm comm, const PFDATA raw_data, Network *network_ptr)
{
    PetscErrorCode ierr;
    PetscMPIInt    rank;
    PetscInt      i, nbuses, nbranches;
    PetscInt      bnum;

    Branch *branches;
    Bus *buses;

    PetscFunctionBegin;
    ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

    nbuses = raw_data.nbus;
    nbranches = raw_data.nbranch;

    network_ptr->nbuses = nbuses;
    network_ptr->nbranches = nbranches;

    ierr = PetscCalloc1(2*nbranches,&(network_ptr->edgelist));CHKERRQ(ierr);
    for (i = 0; i < nbranches; i++) {
        network_ptr->edgelist[2*i] = raw_data.branch[i].internal_i;
        network_ptr->edgelist[2*i + 1] = raw_data.branch[i].internal_j;
    }
    
    ierr = PetscCalloc2(nbuses,&buses,nbranches,&branches);CHKERRQ(ierr);

    for (i = 0; i < nbuses; i++) {
        buses[i].id = raw_data.bus[i].internal_i;
        buses[i].type = raw_data.bus[i].ide;
        buses[i].pinj = 0.0;        
    }

    for (i = 0; i < raw_data.ngen; i++) {
        bnum = raw_data.gen[i].internal_i;
        buses[bnum].pinj += raw_data.gen[i].pg/raw_data.sbase;
    }

    for (i = 0; i < raw_data.nload; i++) {
        bnum = raw_data.load[i].internal_i;
        buses[bnum].pinj -= raw_data.load[i].pl/raw_data.sbase;
    }

    for (i = 0; i < nbranches; i++) {
        branches[i].id = i;
        branches[i].length = 10.0;
        branches[i].b = 400.0;
        branches[i].voltage = 1.0;
        branches[i].h = 1.0;
        branches[i].w_0 = 1.0;
        branches[i].npoints = 20;
        branches[i].pg = 0.0;
    }
    network_ptr->buses = buses;
    network_ptr->branches = branches;

    return ierr;
}

PetscErrorCode FreeNetwork(Network *network_ptr)
{
    PetscErrorCode ierr;
    ierr = PetscFree(network_ptr->edgelist);
    ierr = PetscFree(network_ptr->buses);
    ierr = PetscFree(network_ptr->branches);

    return ierr;
}

// MAIN
int main(int argc, char **argv) {

    PetscErrorCode ierr;
    PetscMPIInt    size, rank;
    PetscInt       KeyBus, KeyBranch;
    PetscInt       i, eStart, eEnd, vStart, vEnd;
    Vec            X, Finit;
    Mat            J;
    DM             networkdm;
    SNES           snes;
    TS             ts;

    // Parameters
    PetscInt       steps=300;
    Network        network_case;
    char           pfdata_file[PETSC_MAX_PATH_LEN]="data/case9.m";
    PFDATA         pfdata;

    ierr = PetscInitialize(&argc,&argv,"waveoptions",help);if (ierr) return ierr;
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
    ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size);CHKERRQ(ierr);

    // Runtime options
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD,NULL,"Simulation Settings","");CHKERRQ(ierr);
    {
        ierr = PetscOptionsGetString(NULL,NULL,"-pfdata",pfdata_file,PETSC_MAX_PATH_LEN-1,NULL);CHKERRQ(ierr);
    }
    ierr = PetscOptionsEnd();CHKERRQ(ierr);

    ierr = DMNetworkCreate(PETSC_COMM_WORLD,&networkdm);CHKERRQ(ierr);
    ierr = DMNetworkRegisterComponent(networkdm,"branchstruct",sizeof(struct _p_Branch),
            &KeyBranch);CHKERRQ(ierr);
    ierr = DMNetworkRegisterComponent(networkdm,"busstruct",sizeof(struct _p_Bus),
            &KeyBus);CHKERRQ(ierr);
    //ierr = CreateNetwork(PETSC_COMM_WORLD, &network_case);CHKERRQ(ierr);

    if (!rank) {
        ierr = PFReadMatPowerData(&pfdata,pfdata_file);CHKERRQ(ierr);
        ierr = CreateFromData(PETSC_COMM_WORLD, pfdata, &network_case);
    }

    ierr = DMNetworkSetNumSubNetworks(networkdm,PETSC_DECIDE,1);CHKERRQ(ierr);
    ierr = DMNetworkAddSubnetwork(networkdm,"",network_case.nbuses, network_case.nbranches,network_case.edgelist,NULL);CHKERRQ(ierr);


    //ierr = DMNetworkSetSizes(networkdm,1,&(network_case.nbuses),&(network_case.nbranches),0,NULL);CHKERRQ(ierr);
    //ierr = DMNetworkSetEdgeList(networkdm,&(network_case.edgelist),NULL);CHKERRQ(ierr);
    ierr = DMNetworkLayoutSetUp(networkdm);CHKERRQ(ierr);

    if (!rank) {
        ierr = DMNetworkGetEdgeRange(networkdm,&eStart,&eEnd);CHKERRQ(ierr);
        for (i = eStart; i < eEnd; i++) {
          ierr = DMNetworkAddComponent(networkdm,i,KeyBranch,&network_case.branches[i-eStart],
                          2*network_case.branches[i-eStart].npoints);CHKERRQ(ierr);
          //ierr = DMNetworkAddNumVariables(networkdm,i,2*network_case.branches[i-eStart].npoints);CHKERRQ(ierr);
        }

        ierr = DMNetworkGetVertexRange(networkdm,&vStart,&vEnd);CHKERRQ(ierr);
        for (i = vStart; i < vEnd; i++) {
          ierr = DMNetworkAddComponent(networkdm,i,KeyBus,&network_case.buses[i-vStart],
                          2);CHKERRQ(ierr);
          /* Add number of variables */
          //ierr = DMNetworkAddNumVariables(networkdm,i,2);CHKERRQ(ierr);
        }
    }

    /* Network partitioning and distribution of data */
    ierr = DMSetUp(networkdm);CHKERRQ(ierr);
    ierr = DMNetworkDistribute(&networkdm,0);CHKERRQ(ierr);

    /* Create vector */
    ierr = DMCreateGlobalVector(networkdm,&X);CHKERRQ(ierr);
    //ierr = VecSet(X,5.0);

    /* Initialize vector. We write it as a non-linear problem */
    ierr = VecDuplicate(X,&Finit);CHKERRQ(ierr);
    
    /* Set up Jacobian */
    ierr = DMCreateMatrix(networkdm,&J);CHKERRQ(ierr);

    /* SNES solver environment */
    ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
    ierr = SNESSetOptionsPrefix(snes, "init_");CHKERRQ(ierr);
    ierr = SNESSetDM(snes,networkdm);CHKERRQ(ierr);
    ierr = SNESSetFunction(snes,Finit,InitFunction,NULL);CHKERRQ(ierr);
    ierr = SNESSetJacobian(snes,J,J,InitJacobian,NULL);CHKERRQ(ierr);
    ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
    ierr = SNESSolve(snes,NULL,X);CHKERRQ(ierr);

    ierr = PostProcessInitialization(networkdm, X);CHKERRQ(ierr);
    /* Apply perturbation */
    ierr = SetFault(networkdm, 1, 2.0);CHKERRQ(ierr);

    /* TS solver environment */
    ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
    ierr = TSSetDM(ts,networkdm);CHKERRQ(ierr);
    ierr = TSSetIFunction(ts,NULL,WAVEIFunction,NULL);CHKERRQ(ierr);
    ierr = TSSetIJacobian(ts,J,J,WAVEIJacobian,NULL);CHKERRQ(ierr);
    ierr = TSSetMaxSteps(ts,steps);CHKERRQ(ierr);
    ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
    ierr = TSSetTimeStep(ts,0.01);CHKERRQ(ierr);
    ierr = TSSetType(ts,TSBEULER);CHKERRQ(ierr);
    ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
    ierr = TSSolve(ts,X);CHKERRQ(ierr);

    /* Free memory */
    ierr = VecDestroy(&X);CHKERRQ(ierr);
    ierr = VecDestroy(&Finit);CHKERRQ(ierr);
    ierr = MatDestroy(&J);CHKERRQ(ierr);
    ierr = SNESDestroy(&snes);CHKERRQ(ierr);
    ierr = TSDestroy(&ts);CHKERRQ(ierr);

    if (!rank) ierr = FreeNetwork(&network_case);CHKERRQ(ierr);
    ierr = DMDestroy(&networkdm);CHKERRQ(ierr);
    ierr = PetscFinalize();
    return ierr;

}

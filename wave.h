#ifndef WAVE_H
#define WAVE_H

#include <stdlib.h>
#include <petscdmnetwork.h>
#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscts.h>

#define BUS_TYPE_SLACK 3
#define BUS_TYPE_PV 2
#define BUS_TYPE_PQ 1

#define W_IDX 0
#define P_IDX 1

// DATA STRUCTURES
struct _p_Branch
{
    PetscInt    id;
    PetscReal   length; // lenght of the line (miles)
    PetscReal   b; // susceptance per unit lenght (p.u/miles)
    PetscReal   voltage; // voltage, which we assume uniform
    PetscReal   h; // Inertia (we will do a function in the future);
    PetscReal   w_0; // frequency
    PetscReal   pg;

    /* These parameters are about numerical solution */
    PetscInt    npoints;

} PETSC_ATTRIBUTEALIGNED(sizeof(PetscScalar));
typedef struct _p_Branch Branch;

struct _p_Bus
{
    PetscInt    id;
    PetscInt    type;
    PetscScalar pinj; // Injected power (positive for injection into network)
} PETSC_ATTRIBUTEALIGNED(sizeof(PetscScalar));
typedef struct _p_Bus Bus;

struct _p_Network
{
    MPI_Comm comm;
    PetscInt nedge, nvertex;
    PetscInt Nedge, Nvertex;
    PetscInt *edgelist;

    Bus *buses;
    PetscInt nbuses;

    Branch *branches;
    PetscInt nbranches;

} PETSC_ATTRIBUTEALIGNED(sizeof(PetscScalar));
typedef struct _p_Network Network;

struct _p_Field
{
    PetscScalar p;
    PetscScalar w;
} PETSC_ATTRIBUTEALIGNED(sizeof(PetscScalar));
typedef struct _p_Field Field;

///////////////////////////////
// Power Flow Data Structures//
///////////////////////////////
#define MAXLINE 1000
#define REF_BUS 3
#define PV_BUS 2
#define PQ_BUS 1
#define ISOLATED_BUS 4
#define NGEN_AT_BUS_MAX 15
#define NLOAD_AT_BUS_MAX 1

/* 2. Bus data */
/* 11 columns */
struct _p_VERTEX_Power{
  PetscInt      bus_i; /* Integer bus number .. used by some formats like Matpower */
  char          i[20]; /* Bus Number */
  char          name[20]; /* Bus Name */
  PetscScalar   basekV; /* Bus Base kV */
  PetscInt      ide; /* Bus type code */
  PetscScalar   gl; /* Active component of shunt admittance to ground */
  PetscScalar   bl; /* Reactive component of shunt admittance to ground */
  PetscInt      area; /* Area number */
  PetscInt      zone; /* Zone number */
  PetscScalar   vm; /* Bus voltage magnitude; in pu */
  PetscScalar   va; /* Bus voltage phase angle */
  PetscInt      owner; /* Owner number */
  PetscInt      internal_i; /* Internal Bus Number */
  PetscInt      ngen; /* Number of generators incident at this bus */
  PetscInt      gidx[NGEN_AT_BUS_MAX]; /* list of inndices for accessing the generator data in GEN structure */
  PetscInt      nload;
  PetscInt      lidx[NLOAD_AT_BUS_MAX];
} PETSC_ATTRIBUTEALIGNED(PetscMax(sizeof(double),sizeof(PetscScalar)));

typedef struct _p_VERTEX_Power *VERTEX_Power;

/* 3. Load data */
/* 12 columns */
struct _p_LOAD{
  PetscInt      bus_i; /* Bus number */
  char          i[20]; /* Bus Number or extended bus name*/
  char          id[20]; /* Load identifier, in case of multiple loads. 1 by default */
  PetscInt      status; /* Load status */
  PetscInt      area; /* Area to which load is assigned */
  PetscInt      zone; /* Zone to which load is assigned */
  PetscScalar   pl; /* Active power component of constant MVA load */
  PetscScalar   ql; /* Reactive power component of constant MVA load */
  PetscScalar   ip; /* Active power component of constant current load: MW pu V */
  PetscScalar   iq; /* Reactive power component of constant current load: Mvar pu V */
  PetscScalar   yp; /* Active power component of constant admittance load: MW pu V */
  PetscScalar   yq; /* Reactive power component of constant admittance load: Mvar pu V */
  PetscScalar   scale_load;
  PetscInt      owner; /* Owner number */
  PetscInt      internal_i; /* Internal Bus Number */
} PETSC_ATTRIBUTEALIGNED(PetscMax(sizeof(double),sizeof(PetscScalar)));

typedef struct _p_LOAD *LOAD;

/* 4. Generator data */
/* 20+ columns */
/******* 20, USING ONLY 1 OWNER's WORTH OF DATA. COME BACK TO THIS LATER, if necessary ******/
struct _p_GEN{
  PetscInt      bus_i;
  char          i[20]; /* Bus Number or extended bus name*/
  char          id[20]; /* Generator identifier, in case of multiple generators at same bus. 1 by default */
  PetscScalar   pg; /* Generator active power output */
  PetscScalar   qg; /* Generator reactive power output */
  PetscScalar   qt; /* Maximum reactive power output: Mvar */
  PetscScalar   qb; /* Minimum reactive power output: Mvar */
  PetscScalar   vs; /* Regulated voltage setpoint: pu */
  PetscInt      ireg; /* Remote bus number/identifier */
  PetscScalar   mbase; /* MVA base of the machine */
  PetscScalar   zr; /* Complex machine impedance ZSOURCE in pu on mbase */
  PetscScalar   zx; /* ----------------------"------------------------- */
  PetscScalar   rt; /* Step-up transformer impedance XTRAN in pu on mbase */
  PetscScalar   xt; /* -----------------------"-------------------------- */
  PetscScalar   gtap; /* Step-up transformer turns ratio */
  PetscInt      status; /* Machine status */
  PetscScalar   rmpct; /* Mvar % required to hold voltage at remote bus */
  PetscScalar   pt; /* Gen max active power output: MW */
  PetscScalar   pb; /* Gen min active power output: MW */
  PetscInt      o1; /* Owner number */
  PetscScalar   f1; /* Fraction of ownership */
  PetscScalar   scale_gen;
  PetscInt      internal_i; /* Internal Bus Number */
} PETSC_ATTRIBUTEALIGNED(PetscMax(sizeof(double),sizeof(PetscScalar)));

typedef struct _p_GEN *GEN;

/* 17+ columns */
struct _p_EDGE_Power{
  PetscInt      fbus;
  PetscInt      tbus;
  char          i[20]; /* Bus Number or extended bus name*/
  char          j[20]; /* Bus Number or extended bus name*/
  char          ckt[20]; /* Circuit identifier. 1 by default */
  PetscScalar   r; /* Branch resistance: pu */
  PetscScalar   x; /* Branch reactance: pu */
  PetscScalar   b; /* Branch charging susceptance: pu */
  PetscScalar   rateA; /* rate A in MVA */
  PetscScalar   rateB; /* rate B in MVA */
  PetscScalar   rateC; /* rate C in MVA */
  PetscScalar   tapratio;
  PetscScalar   phaseshift;
  PetscScalar   gi; /* Complex admittance at 'i' end: pu */
  PetscScalar   bi; /* Complex admittance at 'i' end: pu */
  PetscScalar   gj; /* Complex admittance at 'j' end: pu */
  PetscScalar   bj; /* Complex admittance at 'j' end: pu */
  PetscInt      status; /* Service status */
  PetscScalar   length; /* Line length */
  PetscInt      o1; /* Owner number */
  PetscScalar   f1; /* Fraction of ownership */
  PetscScalar   yff[2],yft[2],ytf[2],ytt[2]; /* [G,B] */
  PetscInt      internal_i; /* Internal From Bus Number */
  PetscInt      internal_j; /* Internal To Bus Number */
} PETSC_ATTRIBUTEALIGNED(PetscMax(sizeof(double),sizeof(PetscScalar)));

typedef struct _p_EDGE_Power *EDGE_Power;

/* PTI format data structure */
typedef struct{
  PetscScalar sbase; /* System base MVA */
  PetscInt    nbus,ngen,nbranch,nload; /* # of buses,gens,branches, and loads (includes elements which are
                                          out of service */
  VERTEX_Power bus;
  LOAD         load;
  GEN          gen;
  EDGE_Power   branch;
} PFDATA PETSC_ATTRIBUTEALIGNED(PetscMax(sizeof(double),sizeof(PetscScalar)));



PetscErrorCode InitFunction(SNES snes,Vec X, Vec F, void *appctx);
PetscErrorCode InitJacobian(SNES snes,Vec X, Mat J,Mat Jpre,void *appctx);
PetscErrorCode PostProcessInitialization(DM networkdm, Vec X);
PetscErrorCode WAVEIFunction(TS ts,PetscReal t,Vec X,Vec Xdot,Vec F,void* ctx);
PetscErrorCode SetFault(DM networkdm, PetscInt bus_id, PetscScalar percent);
PetscErrorCode WAVEIJacobian(TS ts,PetscReal t,Vec X,Vec X_t,PetscReal a,Mat J,Mat Jpre,void *appctx);
PetscErrorCode PFReadMatPowerData(PFDATA *pf,char *filename);
#endif
